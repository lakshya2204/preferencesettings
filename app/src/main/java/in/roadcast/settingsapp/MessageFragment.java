package in.roadcast.settingsapp;

import android.os.Bundle;

import androidx.preference.PreferenceFragmentCompat;


public class MessageFragment extends PreferenceFragmentCompat {

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.message_preferences,rootKey);
    }
}
